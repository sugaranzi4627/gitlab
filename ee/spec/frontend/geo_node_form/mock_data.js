// eslint-disable-next-line import/prefer-default-export
export const MOCK_NODE = {
  id: 1,
  name: 'Mock Node',
  url: 'https://mock_node.gitlab.com',
  primary: false,
  internalUrl: '',
  selectiveSyncType: '',
  namespaceIds: [],
  selectiveSyncShards: [],
  reposMaxCapacity: 25,
  filesMaxCapacity: 10,
  verificationMaxCapacity: 100,
  containerRepositoriesMaxCapacity: 10,
  minimumReverificationInterval: 7,
  syncObjectStorage: false,
};
